<?php

//strtolower/upper = cambia la cadena a minúsculas o mayúsculas
//substr_count = devuelve la cantidad de veces que encuentra el substring
function contarVocales ($String){

   $minusculas=strtolower($String);
   $numeroVocales=substr_count($minusculas, "a") + substr_count($minusculas, "e") + substr_count($minusculas, "i") 
   + substr_count($minusculas, "o") +substr_count($minusculas, "u");

   return $numeroVocales;
}



?>