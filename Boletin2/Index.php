<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/CSS" href="Indice1.css"/>
    <title>Index</title>
</head>
<body>

    <div id="todo">
        <div id="header">
        
            <a href="#ejer23" class="button">Ejercicio 23</a>
            <a href="#ejer24" class="button">Ejercicio 24</a>
            <a href="#ejer25" class="button">Ejercicio 25</a>
            <a href="#ejer26" class="button">Ejercicio 26</a>
            <a href="#ejer27" class="button">Ejercicio 27</a>
            <a href="#ejer28" class="button">Ejercicio 28</a>
            <a href="#ejer29" class="button">Ejercicio 29</a>

        </div>


        <div id="main">
            <div id="ejercicios">
                <div id="ejer23">
                    <p>Ejercicio23</p> 
                    <code>

                    function funcionMedia($num1, $num2, $num3, $num4){

                        $media=($num1 + $num2 + $num3 +$num4)/4;

                        return $media;
                        }

                    </code>
                    <br>    
                    <br>
                    <?php
                        
                        include 'Ejercicio23.php';
                        echo funcionMedia (2,4,8,16);
                        echo '<br>';
                        echo '<br>';

                    ?>    
                    <a href="#header" class="button1"> Pincha para volver al principio de la página </a>
                </div>

                <div id="ejer24"> 
                    <p>Ejercicio24</p>
                    <code>

                    function contarVocales ($String){

                        $minusculas=strtolower($String);
                        $numeroVocales=substr_count($minusculas, "a") + substr_count($minusculas, "e") + substr_count($minusculas, "i") 
                        + substr_count($minusculas, "o") +substr_count($minusculas, "u");

                        return $numeroVocales;
                        }

                    </code>
                    <br>   
                    <br>   
                    <?php    

                        include 'Ejercicio24.php';
                        echo contarVocales ('patata');
                        echo '<br>';
                        echo '<br>';

                    ?>   
                    <a href="#header" class="button1">Pincha para volver al principio de la página</a>
                </div>

                <div id="ejer25">
                    <p>Ejercicio25</p>
                    <code>

                    function cuadrado ($char, $numero){

                        for ($i=0; $i < $numero; $i++) { 
                            for ($j=0; $j <$numero ; $j++) { 
                                echo $char;
                            }
                            echo "<br>";
                        }
                    }

                    </code>    
                    <br> 
                    <br>     
                    <?php

                        include 'Ejercicio25.php';
                        echo cuadrado ('*', 6);
                        echo '<br>';
                        echo '<br>';

                    ?>    
                    <a href="#header" class="button1">Pincha para volver al principio de la página</a>
                </div>
                
                <div id="ejer26">
                    <p>Ejercicio26</p>
                    <code>

                    function loteria ($numeroBolas, $numeroBombo){

                        $numeroAleatorio = [];

                        for($i=0; $i<$numeroBolas; $i++){
                            $numeroAleatorio[$i]= mt_rand(0,$numeroBombo);
                        }
                        sort($numeroAleatorio);

                        for ($k=0; $k <$numeroBolas ; $k++) { 
                            echo $numeroAleatorio[$k]." ";
                        }
                    }

                    </code>
                    <br> 
                    <br>     
                    <?php

                        include 'Ejercicio26.php';
                        echo loteria (5,50);
                        echo '<br>';
                        echo '<br>';

                    ?>
                    <a href="#header" class="button1">Pincha para volver al principio de la página</a>
                </div>

                <div id="ejer27">
                    <p>Ejercicio27</p>
                
                    <?php
                    function funcionMedia1 ($num1, $num2, $num3, $num4){
                        $media=($num1 + $num2 + $num3 +$num4)/4;
                        return $media;
                    }
                    
                    echo funcionMedia1 (2,4,8,16);
                    echo '<br>';
                    echo '<br>';

                    function contarVocales1 ($String){

                        $minusculas=strtolower($String);
                        $numeroVocales=substr_count($minusculas, "a") + substr_count($minusculas, "e") + substr_count($minusculas, "i") 
                        + substr_count($minusculas, "o") +substr_count($minusculas, "u");
                    
                        return $numeroVocales;
                    }

                    echo contarVocales1 ('campesino');
                    echo '<br>';
                    echo '<br>';

                    function cuadrado1 ($char, $numero){

                        for ($i=0; $i < $numero; $i++) { 
                            for ($j=0; $j <$numero ; $j++) { 
                                echo $char;
                            }
                            echo "<br>";
                        }
                    }

                    echo cuadrado1 ('[', 3);
                    echo '<br>';
                    echo '<br>';

                    function loteria1 ($numeroBolas, $numeroBombo){

                        $numeroAleatorio = [];

                        for($i=0; $i<$numeroBolas; $i++){
                            $numeroAleatorio[$i]= mt_rand(0,$numeroBombo);
                        }
                        sort($numeroAleatorio);
                        
                        for ($k=0; $k <$numeroBolas ; $k++) { 
                            echo $numeroAleatorio[$k]." ";
                        }
                    }

                    echo loteria1 (5,50);
                    echo '<br>';
                    echo '<br>';

                    ?>
                    <a href="#header" class="button1">Pincha para volver al principio de la página</a>
                </div>
                
                <div id="ejer28">
                    <p>Ejercicio28</p>
                    <code>

                        include 'funciones.inc.php';

                    </code>
                    <br> 
                    <br>     
                    <?php

                        include 'Ejercicio28.php';
                        echo funcionMedia2 (2,2,2,2);
                        echo '<br>'. '<br>' ;
                        echo contarVocales2 ('botella');
                        echo '<br>'. '<br>';
                        echo cuadrado2 ('^', 4);
                        echo '<br>';
                        echo loteria2 (5,50);
                        echo '<br>';
                        echo '<br>';
                    ?>    
                    <a href="#header" class="button1">Pincha para volver al principio de la página</a>
                </div>

                <div id="ejer29">
                    <p>Ejercicio29</p>
                    <code>

                        include_once 'funciones.inc.php';

                    </code>
                    <br> 
                    <br>     
                    <?php

                        include 'Ejercicio29.php';
                        echo funcionMedia2 (2,9,10,5);
                        echo '<br>'. '<br>' ;
                        echo contarVocales2 ('elefante');
                        echo '<br>'. '<br>';
                        echo cuadrado2 ('-', 4);
                        echo '<br>';
                        echo loteria2 (5,50);
                        echo '<br>';
                        echo '<br>';

                    ?>
                    <a href="#header" class="button1">Pincha para volver al principio de la página</a>
                </div>
            </div>      
        </div>
    </div>    
</body>
</html>